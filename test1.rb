# 导入所需的库
require 'minitest/autorun'
require 'ostruct' # 引入 OpenStruct

# 定义要测试的函数
def extracted_files
  Dir.glob("#{@shared.export_path}/**/*", File::FNM_DOTMATCH).reject { |f| f =~ %r{.*/\.{1,2}$} }
end

class TestExtractedFiles < Minitest::Test
  def setup
    # 在测试前设置共享的export_path
    @shared = OpenStruct.new(export_path: '/home/ubuntu/uploads')
  end

  def test_extracted_files
    # 调用要测试的函数
    extracted_files = extracted_files()
	puts extracted_files
    # 断言函数的返回值中不包含指定路径
    if extracted_files.include?('/home/ubuntu/uploads/.\nevil')
      puts "测试失败：函数未排除指定路径"
    else
      puts "测试成功：函数成功排除指定路径"
    
    end
  end
end
